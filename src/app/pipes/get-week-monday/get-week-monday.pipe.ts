import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'getWeekMonday'
})
export class GetWeekMondayPipe implements PipeTransform {

  transform(d: any): any {
    d = new Date(d);
    const day = d.getDay(),
      diff = d.getDate() - day + (day === 0 ? -6 : 1);
    return new Date(d.setDate(diff));
  }
}
