import {Component, OnInit} from '@angular/core';
import {AuthService} from './services/auth/auth.service';
import {EmitDataService} from './services/emit-data/emit-data.service';
import {Router} from '@angular/router';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  typeOfUser: string;
  env = environment;

  constructor(public auth: AuthService,
              public router: Router,
              public emitDataService: EmitDataService) {
  }

  ngOnInit(): void {
    this.typeOfUser = localStorage.getItem('userRoles');
    this.emitDataService.data.subscribe(newData => {
      if (newData !== '') {
        this.typeOfUser = newData;
      }
    });
  }

  signOut() {
    localStorage.removeItem('userData');
    localStorage.removeItem('token');
    localStorage.removeItem('userRoles');
    this.router.navigate(['home']);
  }
}
