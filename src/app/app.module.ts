import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AgmCoreModule} from '@agm/core';
import {TrackerMapComponent} from './components/tracker-map/tracker-map.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MAT_DATE_LOCALE,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatOptionModule,
  MatProgressBarModule, MatProgressSpinnerModule,
  MatRadioModule,
  MatSelectModule,
  MatSidenavModule,
  MatStepperModule,
  MatToolbarModule
} from '@angular/material';
import {NgxSpinnerModule} from 'ngx-spinner';
import {RegisterComponent} from './components/register/register.component';
import {RegisterRoutingModule} from './components/register/register-routing-module';
import {ProfileRoutingModule} from './components/profile/profile-routing-module';
import {MyAddedTicketsRoutingModule} from './components/my-added-tickets/my-added-tickets-routing';
import {LoginRoutingModule} from './components/login/login-routing-module';
import {AddTicketRoutingModule} from './components/add-ticket/add-ticket-routing-module';
import {TrackerMapRoutingModule} from './components/tracker-map/tracker-map-routing-module';
import {HttpClientModule} from '@angular/common/http';
import {SlideshowModule} from 'ng-simple-slideshow';
import {DatePipe, KeyValuePipe} from '@angular/common';
import {RegisterBusinessUserCardComponent} from './components/register-business-user-card/register-business-user-card.component';
import {RegisterPrivateUserCardComponent} from './components/register-private-user-card/register-private-user-card.component';
import {AddTicketComponent} from './components/add-ticket/add-ticket.component';
import {AddTicketCardComponent} from './components/add-ticket-card/add-ticket-card.component';
import {DailyRelationComponent} from './components/add-ticket-components/daily-relation/daily-relation.component';
import {EveryDayRelationComponent} from './components/add-ticket-components/every-day-relation/every-day-relation.component';
import {WeeklyRelationComponent} from './components/add-ticket-components/weekly-relation/weekly-relation.component';
import {GetWeekMondayPipe} from './pipes/get-week-monday/get-week-monday.pipe';
import {AuthService} from './services/auth/auth.service';
import {ProfileComponent} from './components/profile/profile.component';
import {MyAddedTicketsComponent} from './components/my-added-tickets/my-added-tickets.component';
import {HomeComponent} from './components/home/home.component';
import {LoginCardComponent} from './components/login-card/login-card.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from './components/login/login.component';
import {TicketCardComponent} from './components/ticket-card/ticket-card.component';
import {DecodePhoneNumberPipe} from './pipes/decode-phone-number/decode-phone-number.pipe';
import {BusinessProfileCardComponent} from './components/business-profile-card/business-profile-card.component';
import {BrowseDestinationCardComponent} from './components/browse-destination-card/browse-destination-card.component';
import {DialogPurchaseTicketComponent} from './dialog-components/dialog-purchase-ticket/dialog-purchase-ticket.component';
import {DialogTicketDetailsComponent} from './dialog-components/dialog-ticket-details/dialog-ticket-details.component';
import {DialogConfirmationComponent} from './dialog-components/dialog-confirmation/dialog-confirmation.component';
import {StepSelectTicketComponent} from './dialog-components/dialog-purchase-ticket/steps/step-select-ticket/step-select-ticket.component';
import {StepPayTicketComponent} from './dialog-components/dialog-purchase-ticket/steps/step-pay-ticket/step-pay-ticket.component';
import {StepEnterPaymentComponent} from './dialog-components/dialog-purchase-ticket/steps/step-enter-payment/step-enter-payment.component';
import {SpinnerComponent} from './components/spinner/spinner.component';
import {PrivateUserProfileCardComponent} from './private-user-profile-card/private-user-profile-card.component';
import {DialogPurchasedTicketViewComponent} from './dialog-components/dialog-purchased-ticket-view/dialog-purchased-ticket-view.component';
import {MyPurchasedTicketsComponent} from './components/my-purchased-tickets/my-purchased-tickets.component';
import {MyPurchasedTicketsRoutingModule} from './components/my-purchased-tickets/my-purchased-tickets-routing-module';
import {PurchasedTicketCardComponent} from './components/purchased-ticket-card/purchased-ticket-card.component';
import {NgxBarcodeModule} from 'ngx-barcode';
import { MessagesFilterComponent } from './components/messages-filter/messages-filter.component';
import { RegisterSuccessComponent } from './components/register-success/register-success.component';
import { PurchasedTicketViewComponent } from './components/purchased-ticket-view/purchased-ticket-view.component';
import { SuccessComponent } from './components/success/success.component';
import {SuccessRoutingModule} from './components/success/success-routing-module';
import { BrowseVehiclesUsingDestinationsComponent } from './components/browse-vehicles-using-destinations/browse-vehicles-using-destinations.component';
import { TrackPersonalVehicleComponent } from './components/track-personal-vehicle/track-personal-vehicle.component';
import { SliderComponent } from './components/slider/slider.component';
import { HomeDescriptionCardComponent } from './components/home-description-card/home-description-card.component';
import { HomeFooterComponent } from './components/home-footer/home-footer.component';
import {GOOGLE_API_KET} from '../environments/environment';
import { BrowseTicketsComponent } from './components/browse-tickets/browse-tickets.component';
import {BrowseTicketsRoutingModule} from './components/browse-tickets/browse-tickets-routing-module';
import { HomeDestinationCardComponent } from './components/home-destination-card/home-destination-card.component';
import { AddTicketStartDestEndDestComponent } from './components/add-ticket-start-dest-end-dest/add-ticket-start-dest-end-dest.component';
import { AddTicketTypeAndDatesComponent } from './components/add-ticket-type-and-dates/add-ticket-type-and-dates.component';
import {PurchaseSuccessComponent} from './components/purchase-success/purchase-success.component';

@NgModule({
  declarations: [
    AppComponent,
    TrackerMapComponent,
    RegisterComponent,
    HomeComponent,
    LoginCardComponent,
    LoginComponent,
    RegisterPrivateUserCardComponent,
    RegisterBusinessUserCardComponent,
    AddTicketComponent,
    AddTicketCardComponent,
    DailyRelationComponent,
    EveryDayRelationComponent,
    WeeklyRelationComponent,
    GetWeekMondayPipe,
    ProfileComponent,
    MyAddedTicketsComponent,
    TicketCardComponent,
    DialogTicketDetailsComponent,
    DialogPurchaseTicketComponent,
    DialogPurchasedTicketViewComponent,
    DecodePhoneNumberPipe,
    BusinessProfileCardComponent,
    BrowseDestinationCardComponent,
    StepSelectTicketComponent,
    StepPayTicketComponent,
    StepEnterPaymentComponent,
    DialogConfirmationComponent,
    SpinnerComponent,
    MyPurchasedTicketsComponent,
    PurchasedTicketCardComponent,
    PrivateUserProfileCardComponent,
    MessagesFilterComponent,
    RegisterSuccessComponent,
    PurchasedTicketViewComponent,
    SuccessComponent,
    BrowseVehiclesUsingDestinationsComponent,
    TrackPersonalVehicleComponent,
    SliderComponent,
    HomeDescriptionCardComponent,
    HomeFooterComponent,
    BrowseTicketsComponent,
    HomeDestinationCardComponent,
    AddTicketStartDestEndDestComponent,
    AddTicketTypeAndDatesComponent,
    PurchaseSuccessComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RegisterRoutingModule,
    LoginRoutingModule,
    TrackerMapRoutingModule,
    AddTicketRoutingModule,
    ProfileRoutingModule,
    MyAddedTicketsRoutingModule,
    MyPurchasedTicketsRoutingModule,
    BrowseTicketsRoutingModule,
    SuccessRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: GOOGLE_API_KET.apiKey,
      libraries: ['places']
    }),
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatGridListModule,
    MatMenuModule,
    MatOptionModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatIconModule,
    FormsModule,
    MatDividerModule,
    MatListModule,
    MatProgressBarModule,
    MatRadioModule,
    MatStepperModule,
    MatDialogModule,
    NgxSpinnerModule,
    NgxBarcodeModule,
    MatSidenavModule,
    MatProgressSpinnerModule,
    SlideshowModule
  ],

  entryComponents: [
    DialogTicketDetailsComponent,
    DialogPurchaseTicketComponent,
    DialogConfirmationComponent,
    DialogPurchasedTicketViewComponent
  ],
  providers: [DatePipe, AuthService, MatDatepickerModule, KeyValuePipe, GetWeekMondayPipe, {provide: MAT_DATE_LOCALE, useValue: 'en-GB'}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
