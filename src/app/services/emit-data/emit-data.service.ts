import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmitDataService {

  private dataForSharing = new BehaviorSubject<any>('');
  data = this.dataForSharing.asObservable();

  constructor() {
  }

  updateData(data: any) {
    this.dataForSharing.next(data);
  }
}
