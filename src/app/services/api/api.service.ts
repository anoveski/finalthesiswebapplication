import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {HOST_CREDENTIALS} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private  httpClient: HttpClient) {
  }

  getDescriptionCardsData() {
    return new Promise(
      (resolve, reject) => {
        this.httpClient.get(HOST_CREDENTIALS.hostURL + '/api/applicationData/getDescriptionCardsData', {
          headers: new HttpHeaders({'Content-Type': 'application/json; charset=UTF-8'})
        })
          .subscribe((data: any[]) => {
            resolve(data);
          }, (error) => {
            reject(error);
          });
      }
    );
  }

  registerAccount(body) {
    return new Promise(
      (resolve, reject) => {
        this.httpClient.post(HOST_CREDENTIALS.hostURL + '/api/register', body, {
          headers: new HttpHeaders({'Content-Type': 'application/json; charset=UTF-8'})
        })
          .subscribe((data: any) => {
            resolve(data);
          }, (error) => {
            reject(error);
          });
      }
    );
  }

  login(body) {
    return new Promise(
      (resolve, reject) => {
        this.httpClient.post(HOST_CREDENTIALS.hostURL + '/api/login', body, {
          headers: new HttpHeaders({'Content-Type': 'application/json; charset=UTF-8'})
        })
          .subscribe((data: any) => {
            resolve(data);
          }, (error) => {
            reject(error);
          });
      }
    );
  }

  addTicket(body) {
    return new Promise(
      (resolve, reject) => {
        this.httpClient.post(HOST_CREDENTIALS.hostURL + '/api/addTicket', body, {
          headers: new HttpHeaders({'Authorization': localStorage.getItem('token')})
        })
          .subscribe((data: any) => {
            resolve(data);
          }, (error) => {
            reject(error);
          });
      }
    );
  }

  getCompanyTickets(body) {
    return new Promise(
      (resolve, reject) => {
        this.httpClient.post(HOST_CREDENTIALS.hostURL + '/api/getCompanyTickets', body, {
          headers: new HttpHeaders({'Authorization': localStorage.getItem('token')})
        })
          .subscribe((data: any) => {
            resolve(data);
          }, (error) => {
            reject(error);
          });
      }
    );
  }

  getMyProfile() {
    return new Promise(
      (resolve, reject) => {
        this.httpClient.get(HOST_CREDENTIALS.hostURL + '/api/getMyProfileData', {
          headers: new HttpHeaders({'Authorization': localStorage.getItem('token')})
        })
          .subscribe((data: any) => {
            resolve(data);
          }, (error) => {
            reject(error);
          });
      }
    );
  }

  getAvailableDestinations() {
    return new Promise(
      (resolve, reject) => {
        this.httpClient.get(HOST_CREDENTIALS.hostURL + '/api/getAvailableDestinations')
          .subscribe((data: any) => {
            resolve(data);
          }, (error) => {
            reject(error);
          });
      }
    );
  }

  browseDestinations(body) {
    return new Promise(
      (resolve, reject) => {
        this.httpClient.post(HOST_CREDENTIALS.hostURL + '/api/browseDestinations', body, {
          headers: new HttpHeaders({'Content-Type': 'application/json; charset=UTF-8'})
        })
          .subscribe((data: any) => {
            resolve(data);
          }, (error) => {
            reject(error);
          });
      }
    );
  }

  purchaseTicket(body) {
    return new Promise(
      (resolve, reject) => {
        this.httpClient.post(HOST_CREDENTIALS.hostURL + '/api/purchaseTicket', body, {
          headers: new HttpHeaders({
            'Authorization': localStorage.getItem('token'),
            'Content-Type': 'application/json; charset=UTF-8'
          })
        })
          .subscribe((data: any) => {
            resolve(data);
          }, (error) => {
            reject(error);
          });
      }
    );
  }

  getPurchasedTickets(body) {
    return new Promise(
      (resolve, reject) => {
        this.httpClient.post(HOST_CREDENTIALS.hostURL + '/api/getPurchasedTickets', body, {
          headers: new HttpHeaders({
            'Authorization': localStorage.getItem('token')
          })
        })
          .subscribe((data: any) => {
            resolve(data);
          }, (error) => {
            reject(error);
          });
      }
    );
  }

  downloadPurchasedTicket(body) {
    return new Promise(
      (resolve, reject) => {
        this.httpClient.post(HOST_CREDENTIALS.hostURL + '/api/downloadTicket', body, {
          observe: 'response',
          headers: new HttpHeaders({
            'Authorization': localStorage.getItem('token'),
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/pdf',
          }), responseType: 'blob'
        })
          .subscribe((data: any) => {
            const contentDisposition = data.headers.get('content-disposition');

            resolve({
              pdf: data.body,
              fileName: JSON.parse(contentDisposition.split(';')[1].trim().split('=').splice('"')[1])
            });
          });
      }
    );
  }
}
