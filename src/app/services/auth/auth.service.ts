import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() {
  }

  loggedIn() {
    if (localStorage.getItem('token') === '' || localStorage.getItem('token') === null) {
      return false;
    } else {
      return true;
    }
  }
}
