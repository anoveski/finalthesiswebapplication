import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivateUserProfileCardComponent } from './private-user-profile-card.component';

describe('PrivateUserProfileCardComponent', () => {
  let component: PrivateUserProfileCardComponent;
  let fixture: ComponentFixture<PrivateUserProfileCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivateUserProfileCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivateUserProfileCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
