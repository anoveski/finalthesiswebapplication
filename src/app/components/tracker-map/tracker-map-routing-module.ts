import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {TrackerMapComponent} from './tracker-map.component';

const routes: Routes = [
  {
    path: 'trackerMap', component: TrackerMapComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class TrackerMapRoutingModule {
}
