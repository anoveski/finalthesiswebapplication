import {Component, OnInit} from '@angular/core';
import * as socketIo from 'socket.io-client';
import {HOST_CREDENTIALS} from '../../../environments/environment';
import {LocationService} from '../../services/location/location.service';

@Component({
  selector: 'app-tracker-map',
  templateUrl: './tracker-map.component.html',
  styleUrls: ['./tracker-map.component.css']
})
export class TrackerMapComponent implements OnInit {

  typeOfUser: string;
  socket = socketIo(HOST_CREDENTIALS.hostURL);
  latitude: number;
  longitude: number;
  showMarker = false;
  pinTitle: string;
  mapZoom = 8;

  mapLatitude = 41.785930;
  mapLongitude = 21.692989;

  constructor(private locationService: LocationService) {
  }

  ngOnInit() {
    this.typeOfUser = localStorage.getItem('userRoles');
    this.setCurrentPosition();
  }

  showActiveVehicles(event) {
    if (event.startLocation !== '' && event.startLocation !== undefined && event.destinationLocation !== ''
      && event.destinationLocation !== undefined) {

      this.pinTitle = event.startLocation.townOrMunicipality + ', ' + event.startLocation.country.name + ' --> ' +
        event.destinationLocation.townOrMunicipality + ', ' + event.destinationLocation.country.name;
      this.trackVehicleEvent(true);

    } else if (event.selectedVehicle !== '' && event.selectedVehicle !== undefined) {
      this.pinTitle = event.selectedVehicle.name;
      this.trackVehicleEvent(true);
    }
  }

  trackVehicleEvent(startTracking: boolean) {
    this.socket.on('vehicleLocation', data => {
      console.log(data);
      this.showMarker = true;
      this.mapZoom = 18;
      this.latitude = data.lat;
      this.longitude = data.lon;

      if (startTracking === true) {
        this.mapLatitude = data.lat;
        this.mapLongitude = data.lon;
        startTracking = false;
      }
    });
  }

  setCurrentPosition() {
    this.locationService.getCurrentPosition()
      .then((position: any) => {
        this.mapLatitude = position.latitude;
        this.mapLongitude = position.longitude;
        this.mapZoom = 12;
      }).catch((error) => {
      console.log(error);
    });
  }
}
