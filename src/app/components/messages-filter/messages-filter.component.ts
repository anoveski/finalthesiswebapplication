import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-messages-filter',
  templateUrl: './messages-filter.component.html',
  styleUrls: ['./messages-filter.component.css']
})
export class MessagesFilterComponent implements OnInit {

  @Input() selectedFilter: string;
  @Input() messagesFilters: any[];
  @Output() chosenFilter: EventEmitter<object> = new EventEmitter<object>();

  constructor() {
  }

  ngOnInit() {
  }

  selectFilter(selectedFilter) {
    this.chosenFilter.emit(selectedFilter);
  }
}
