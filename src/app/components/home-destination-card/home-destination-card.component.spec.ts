import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeDestinationCardComponent } from './home-destination-card.component';

describe('HomeDestinationCardComponent', () => {
  let component: HomeDestinationCardComponent;
  let fixture: ComponentFixture<HomeDestinationCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeDestinationCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeDestinationCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
