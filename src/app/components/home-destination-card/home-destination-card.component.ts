import {Component, Input, OnInit} from '@angular/core';
import {DialogTicketDetailsComponent} from '../../dialog-components/dialog-ticket-details/dialog-ticket-details.component';
import {MatDialog} from '@angular/material/dialog';
import {BILLING_CURRENCY} from '../../../environments/environment';

@Component({
  selector: 'app-home-destination-card',
  templateUrl: './home-destination-card.component.html',
  styleUrls: ['./home-destination-card.component.css']
})
export class HomeDestinationCardComponent implements OnInit {

  @Input() ticketData: any;

  currency: string = BILLING_CURRENCY.currency;

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
  }


  viewDetails(fullTicket) {
    const dialogRef = this.dialog.open(DialogTicketDetailsComponent, {
      height: '80%',
      width: '80%',
      data: fullTicket
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


}
