import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ApiService} from '../../services/api/api.service';
import {EmitDataService} from '../../services/emit-data/emit-data.service';

@Component({
  selector: 'app-login-card',
  templateUrl: './login-card.component.html',
  styleUrls: ['./login-card.component.css']
})
export class LoginCardComponent implements OnInit {

  f: FormGroup;
  emailError = false;
  passwordError = false;
  disableLoginButton = false;

  constructor(private fb: FormBuilder,
              private apiService: ApiService,
              private emitDataService: EmitDataService,
              private router: Router) {
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.f = this.fb.group({
      username: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ])
    });
  }

  signIn() {
    this.disableLoginButton = true;
    this.apiService.login({
      email: this.f.controls.username.value,
      password: this.f.controls.password.value
    }).then((data: any) => {
      if (data.success !== false) {
        this.disableLoginButton = false;
        localStorage.setItem('userData', JSON.stringify(data.userData));
        localStorage.setItem('token', data.token);
        localStorage.setItem('userRoles', data.role);
        this.emitDataService.updateData(data.role);
        this.router.navigate(['home']);
      }
    }).catch(e => {
      if (e.error.success === false) {
        this.disableLoginButton = false;
        this.emailError = e.error.errorLocation.emailError;
        this.passwordError = e.error.errorLocation.passwordError;
      }
    });
  }

  registerAsCompany() {
    this.router.navigate(['register/', 'BUSINESS_USER']);
  }

  registerAsPassenger() {
    this.router.navigate(['register/', 'PRIVATE_USER']);
  }


}
