import {Component, Input, OnInit} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent implements OnInit {

  @Input() spinnerType: string;
  @Input() comment: string;

  constructor(private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
  }

  showSpinner() {
    this.spinner.show();
  }

  closeSpinner() {
    this.spinner.hide();
  }
}
