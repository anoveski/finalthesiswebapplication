import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterPrivateUserCardComponent } from './register-private-user-card.component';

describe('RegisterPrivateUserCardComponent', () => {
  let component: RegisterPrivateUserCardComponent;
  let fixture: ComponentFixture<RegisterPrivateUserCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterPrivateUserCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterPrivateUserCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
