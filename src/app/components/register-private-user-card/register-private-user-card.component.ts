import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-register-private-user-card',
  templateUrl: './register-private-user-card.component.html',
  styleUrls: ['./register-private-user-card.component.css']
})
export class RegisterPrivateUserCardComponent implements OnInit {

  @Input() disableRegisterButton: boolean;
  @Input() invalidCredentials: boolean;
  @Output() registerAccountEvent: EventEmitter<object> = new EventEmitter<object>();

  f: FormGroup;

  genders = [
    {value: 'male-0', viewValue: 'Male'},
    {value: 'female-1', viewValue: 'Female'},
  ];

  minDate = new Date(1900, 0, 1);
  maxDate = new Date(Date.now());

  constructor(private fb: FormBuilder,
              private datePipe: DatePipe) {
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.f = this.fb.group({
      firstName: new FormControl('', [
        Validators.required
      ]),
      lastName: new FormControl('', [
        Validators.required
      ]),
      gender: new FormControl('', [
        Validators.required
      ]),
      date_of_birth: new FormControl('', [
        Validators.required
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ])
    });
  }

  registerAccount() {
    if (this.f.controls.firstName.value === '' && this.f.controls.lastName.value === '' && this.f.controls.gender.value === '' &&
      this.f.controls.email.value === '' && this.f.controls.password.value === '') {
      this.invalidCredentials = true;
    } else {
      this.registerAccountEvent.next({
        role: 'PRIVATE_USER',
        firstName: this.f.controls.firstName.value,
        lastName: this.f.controls.lastName.value,
        gender: this.f.controls.gender.value,
        date_of_birth: this.datePipe.transform(this.f.controls.date_of_birth.value, 'MM/dd/yyyy'),
        email: this.f.controls.email.value,
        password: this.f.controls.password.value
      });
    }
  }
}
