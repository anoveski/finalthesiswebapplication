import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasedTicketCardComponent } from './purchased-ticket-card.component';

describe('PurchasedTicketCardComponent', () => {
  let component: PurchasedTicketCardComponent;
  let fixture: ComponentFixture<PurchasedTicketCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasedTicketCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasedTicketCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
