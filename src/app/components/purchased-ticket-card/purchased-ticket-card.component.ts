import {Component, Input, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {DialogPurchasedTicketViewComponent} from '../../dialog-components/dialog-purchased-ticket-view/dialog-purchased-ticket-view.component';
import {BILLING_CURRENCY} from '../../../environments/environment';

@Component({
  selector: 'app-purchased-ticket-card',
  templateUrl: './purchased-ticket-card.component.html',
  styleUrls: ['./purchased-ticket-card.component.css']
})
export class PurchasedTicketCardComponent implements OnInit {

  @Input() ticket: any;

  currency: string = BILLING_CURRENCY.currency;

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  viewTicket() {
    const dialogRef = this.dialog.open(DialogPurchasedTicketViewComponent, {
      width: '790px',
      height: '400px',
      data: {ticketData: this.ticket}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}
