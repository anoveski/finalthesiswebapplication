import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyAddedTicketsComponent } from './my-added-tickets.component';

describe('MyAddedTicketsComponent', () => {
  let component: MyAddedTicketsComponent;
  let fixture: ComponentFixture<MyAddedTicketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyAddedTicketsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyAddedTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
