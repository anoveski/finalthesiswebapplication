import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {MyAddedTicketsComponent} from './my-added-tickets.component';

const routes: Routes = [
  {
    path: 'myAddedTickets', component: MyAddedTicketsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class MyAddedTicketsRoutingModule {
}
