import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api/api.service';

@Component({
  selector: 'app-my-added-tickets',
  templateUrl: './my-added-tickets.component.html',
  styleUrls: ['./my-added-tickets.component.css']
})
export class MyAddedTicketsComponent implements OnInit {

  myTickets: any[];
  selectedFilter = 'active';

  myAddedTicketsFilters: any[] = [
    {value: 'active', viewValue: 'Active Tickets'},
    {value: 'inactive', viewValue: 'Inactive Tickets'},
    {value: 'all', viewValue: 'All'},
    {value: 'weekly', viewValue: 'Weekly'},
    {value: 'select_days', viewValue: 'Select Days'},
    {value: 'every_day', viewValue: 'Every Day'}
  ];

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
    console.log(this.selectedFilter);
    this.getTickets(this.selectedFilter);
  }

  getTickets(chosenFilter) {
    console.log(chosenFilter);
    this.myTickets = undefined;
    this.apiService.getCompanyTickets({
      ticketType: chosenFilter
    })
      .then((data: any[]) => {
        console.log(data);
        this.myTickets = data;
      })
      .catch(e => {
        console.log(e);
      });
  }
}
