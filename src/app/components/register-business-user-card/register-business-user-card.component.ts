import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {countries} from '../../../additional-data/list-of-countries/countries';

@Component({
  selector: 'app-register-business-user-card',
  templateUrl: './register-business-user-card.component.html',
  styleUrls: ['./register-business-user-card.component.css']
})
export class RegisterBusinessUserCardComponent implements OnInit {

  @Input() disableRegisterButton: boolean;
  @Input() invalidCredentials: boolean;

  @Output() registerAccountEvent: EventEmitter<object> = new EventEmitter<object>();

  f: FormGroup;

  countriesList = countries;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.f = this.fb.group({
      companyName: new FormControl('', [
        Validators.required
      ]),
      companyPhoneNumber: new FormControl('', [
        Validators.required
      ]),
      country: new FormControl({}, [
        Validators.required
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)
      ]),
    });
  }

  registerBusinessAccount() {
    if (this.f.controls.companyName.value !== '' && this.f.controls.companyPhoneNumber.value !== '' &&
      this.f.controls.email.value !== '' && this.f.controls.password.value !== '') {
      this.registerAccountEvent.next({
        role: 'BUSINESS_USER',
        companyName: this.f.controls.companyName.value,
        companyPhoneNumber: this.f.controls.companyPhoneNumber.value,
        country: this.countriesList.find(country => country.code === this.f.controls.country.value),
        email: this.f.controls.email.value,
        password: this.f.controls.password.value
      });
    } else {
      this.invalidCredentials = true;
    }
  }
}
