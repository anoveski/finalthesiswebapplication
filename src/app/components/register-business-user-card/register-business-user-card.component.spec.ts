import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterBusinessUserCardComponent } from './register-business-user-card.component';

describe('RegisterBusinessUserCardComponent', () => {
  let component: RegisterBusinessUserCardComponent;
  let fixture: ComponentFixture<RegisterBusinessUserCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterBusinessUserCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterBusinessUserCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
