import {AfterViewInit, Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {WeeklyRelationComponent} from '../add-ticket-components/weekly-relation/weekly-relation.component';
import {EveryDayRelationComponent} from '../add-ticket-components/every-day-relation/every-day-relation.component';
import {DailyRelationComponent} from '../add-ticket-components/daily-relation/daily-relation.component';
import {countries} from '../../../additional-data/list-of-countries/countries';

@Component({
  selector: 'app-add-ticket-card',
  templateUrl: './add-ticket-card.component.html',
  styleUrls: ['./add-ticket-card.component.css']
})
export class AddTicketCardComponent implements AfterViewInit {

  @Input() disableSubmitButton: boolean;
  @Output() addTicketEvent: EventEmitter<object> = new EventEmitter<object>();

  @ViewChild(WeeklyRelationComponent) weeklyRelation;
  @ViewChild(DailyRelationComponent) everyDayRelation;
  @ViewChild(EveryDayRelationComponent) dailyRelation;

  f: FormGroup;
  typeOfTransportVehicles: any;
  typesOfRelation: any;
  countriesList = countries;

  constructor(private fb: FormBuilder) {
    this.typeOfTransportVehicles = [
      {
        value: 'Bus',
        label: 'Bus'
      }, {
        value: 'Minibus',
        label: 'Minibus'
      }, {
        value: 'Train',
        label: 'Train'
      }, {
        value: 'Car',
        label: 'Car'
      }, {
        value: 'Other',
        label: 'Other'
      }];

    this.typesOfRelation = [
      {
        value: 'weekly',
        label: 'Weekly'
      },
      {
        value: 'select_days',
        label: 'Select days'
      },
      {
        value: 'every_day',
        label: 'Every day'
      }
    ];
    this.buildForm();
  }

  ngAfterViewInit() {
  }

  buildForm() {
    this.f = this.fb.group({
      startLocationCountryCode: new FormControl('', [
        Validators.required
      ]),
      endLocationCountryCode: new FormControl('', [
        Validators.required
      ]),
      startTownOrMunicipality: new FormControl('', [
        Validators.required
      ]),
      endTownOrMunicipality: new FormControl('', [
        Validators.required
      ]),
      typeOfTransportationVehicle: new FormControl('', [
        Validators.required
      ]),
      availableSeats: new FormControl(null, [
        Validators.required
      ]),
      typeOfRelation: new FormControl('', [
        Validators.required
      ]),
      oneWayPrice: new FormControl(null, [
        Validators.required
      ]),
      returnTicketPrice: new FormControl(null, [
        Validators.required
      ]),
      oneWayTicket: new FormControl(false,
        Validators.required
      ),
      returnTicket: new FormControl(false,
        Validators.required
      ),
    });
  }

  submit() {
    this.addTicketEvent.next({
      startLocation: {
        townOrMunicipality: this.f.controls.startTownOrMunicipality.value,
        country: this.countriesList.find(country => country.code === this.f.controls.startLocationCountryCode.value)
      },
      destinationLocation: {
        townOrMunicipality: this.f.controls.endTownOrMunicipality.value,
        country: this.countriesList.find(country => country.code === this.f.controls.endLocationCountryCode.value)
      },
      typeOfTransportationVehicle: this.f.controls.typeOfTransportationVehicle.value,
      availableSeats: this.f.controls.availableSeats.value,
      oneWayPrice: this.f.controls.oneWayPrice.value,
      returnTicketPrice: this.f.controls.returnTicketPrice.value,
      oneWayTicket: this.f.controls.oneWayTicket.value,
      returnTicket: this.f.controls.returnTicket.value,
      typeOfRelation: this.f.controls.typeOfRelation.value,
      relationData: {
        weeklyRelation: (this.f.controls.typeOfRelation.value === 'weekly' ? this.weeklyRelation.takeWeeklyRelationResult() : null),
        dailyRelation: (this.f.controls.typeOfRelation.value === 'select_days' ? this.everyDayRelation.takeDailyRelationResult() : null),
        everyDayRelation: (this.f.controls.typeOfRelation.value === 'every_day' ? this.dailyRelation.takeEveryDayRelationResult() : null)
      }
    });
  }
}
