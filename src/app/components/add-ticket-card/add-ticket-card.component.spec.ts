import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTicketCardComponent } from './add-ticket-card.component';

describe('AddTicketCardComponent', () => {
  let component: AddTicketCardComponent;
  let fixture: ComponentFixture<AddTicketCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTicketCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTicketCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
