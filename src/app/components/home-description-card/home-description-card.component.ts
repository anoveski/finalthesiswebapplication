import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-home-description-card',
  templateUrl: './home-description-card.component.html',
  styleUrls: ['./home-description-card.component.css']
})
export class HomeDescriptionCardComponent implements OnInit {

  @Input() cardData: any;

  constructor() {
  }

  ngOnInit() {
  }

}
