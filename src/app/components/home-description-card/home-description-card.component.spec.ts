import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeDescriptionCardComponent } from './home-description-card.component';

describe('HomeDescriptionCardComponent', () => {
  let component: HomeDescriptionCardComponent;
  let fixture: ComponentFixture<HomeDescriptionCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeDescriptionCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeDescriptionCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
