import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-business-profile-card',
  templateUrl: './business-profile-card.component.html',
  styleUrls: ['./business-profile-card.component.css']
})
export class BusinessProfileCardComponent implements OnInit {

  @Input() businessProfileData: any;

  constructor() { }

  ngOnInit() {
  }

}
