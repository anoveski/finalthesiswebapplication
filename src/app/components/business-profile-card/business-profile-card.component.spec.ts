import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessProfileCardComponent } from './business-profile-card.component';

describe('BusinessProfileCardComponent', () => {
  let component: BusinessProfileCardComponent;
  let fixture: ComponentFixture<BusinessProfileCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusinessProfileCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessProfileCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
