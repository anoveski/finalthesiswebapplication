import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-purchase-success',
  templateUrl: './purchase-success.component.html',
  styleUrls: ['./purchase-success.component.css']
})
export class PurchaseSuccessComponent implements OnInit {

  @Input() ticket: any;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.ticket = JSON.parse(this.route.snapshot.queryParams.ticketData);
  }
}
