import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {PurchaseSuccessComponent} from './purchase-success.component';

const routes: Routes = [
  {
    path: 'purchaseSuccess', component: PurchaseSuccessComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PurchaseSuccessRoutingModule {
}
