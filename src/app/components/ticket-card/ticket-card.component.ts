import {Component, Input, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {DialogTicketDetailsComponent} from '../../dialog-components/dialog-ticket-details/dialog-ticket-details.component';
import {BILLING_CURRENCY} from '../../../environments/environment';

@Component({
  selector: 'app-ticket-card',
  templateUrl: './ticket-card.component.html',
  styleUrls: ['./ticket-card.component.css']
})
export class TicketCardComponent implements OnInit {

  @Input() ticketData: any;

  currency: string = BILLING_CURRENCY.currency;

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
    console.log(this.ticketData.returnTicketPrice);
  }

  viewDetails(fullTicket) {
    const dialogRef = this.dialog.open(DialogTicketDetailsComponent, {
      height: '80%',
      width: '80%',
      data: fullTicket
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}
