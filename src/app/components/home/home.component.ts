import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api/api.service';
import {LocationService} from '../../services/location/location.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  availableDestinations: any;
  noAvailableTickets = false;
  descriptionCardsData: any[];
  municipalityName: string;
  countryCode: string;
  destinationsFromCurrentLocation: any[] = [];

  constructor(private apiService: ApiService,
              private locationService: LocationService) {
  }

  ngOnInit() {
    this.apiService.getAvailableDestinations()
      .then((data) => {
        this.availableDestinations = data;
      }).catch((e) => {
      console.log(e);
    });

    this.apiService.getDescriptionCardsData()
      .then((data: any[]) => {
        this.descriptionCardsData = data;
      }).catch((e) => {
      console.log(e);
    });

    this.locationService.getLocationData()
      .then((data: any) => {
        this.municipalityName = data.results[0].address_components[0].long_name;
        this.countryCode = data.results[0].address_components[1].short_name;

        this.apiService.browseDestinations({
          townOrMunicipality: this.municipalityName.replace('Municipality of ', ''),
          countryCode: this.countryCode
        }).then((destinations: any[]) => {
          this.destinationsFromCurrentLocation = destinations.slice(0, 4);
        }).catch((error) => {
          console.log(error);
        });
      }).catch(error => {
      console.log(error);
    });
  }

  geAvailableTicketsValue(event) {
    this.noAvailableTickets = event;
  }
}
