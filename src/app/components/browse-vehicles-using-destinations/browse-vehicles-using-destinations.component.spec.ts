import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowseVehiclesUsingDestinationsComponent } from './browse-vehicles-using-destinations.component';

describe('BrowseVehiclesUsingDestinationsComponent', () => {
  let component: BrowseVehiclesUsingDestinationsComponent;
  let fixture: ComponentFixture<BrowseVehiclesUsingDestinationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrowseVehiclesUsingDestinationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrowseVehiclesUsingDestinationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
