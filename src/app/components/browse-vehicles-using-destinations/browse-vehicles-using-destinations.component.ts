import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ApiService} from '../../services/api/api.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-browse-vehicles-using-destinations',
  templateUrl: './browse-vehicles-using-destinations.component.html',
  styleUrls: ['./browse-vehicles-using-destinations.component.css']
})
export class BrowseVehiclesUsingDestinationsComponent implements OnInit {

  @Output() browseVehicles: EventEmitter<object> = new EventEmitter<object>();

  availableDestinations: any;
  f: FormGroup;

  constructor(private apiService: ApiService,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.apiService.getAvailableDestinations()
      .then((data) => {
        this.availableDestinations = data;
        console.log(this.availableDestinations);
      }).catch((e) => {
      console.log(e);
    });

    this.buildForm();
  }

  buildForm() {
    this.f = this.fb.group({
      startLocation: new FormControl('', [
        Validators.required
      ]),
      destinationLocation: new FormControl('', [
        Validators.required
      ]),
    });
  }

  searchVehicle() {
    this.browseVehicles.next({
      startLocation: this.f.controls.startLocation.value,
      destinationLocation: this.f.controls.destinationLocation.value
    });
  }
}
