import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api/api.service';

@Component({
  selector: 'app-my-purchased-tickets',
  templateUrl: './my-purchased-tickets.component.html',
  styleUrls: ['./my-purchased-tickets.component.css']
})
export class MyPurchasedTicketsComponent implements OnInit {

  typeOfUser: string;
  myTickets: any[];
  selectedFilter: string;
  myAddedMessagesFilters: any[];

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
    this.typeOfUser = localStorage.getItem('userRoles');

    this.selectedFilter = 'active_purchased_all';

    if (this.typeOfUser === 'PRIVATE_USER') {

      this.myAddedMessagesFilters = [
        {value: 'active_purchased_all', viewValue: 'Purchased tickets'},
        {value: 'inactive_tickets', viewValue: 'Purchased tickets history'},
        {value: 'today_active_tickets', viewValue: 'Today`s tickets'}
      ];
    } else if (this.typeOfUser === 'BUSINESS_USER') {
      this.myAddedMessagesFilters = [
        {value: 'active_purchased_all', viewValue: 'Active sold tickets'},
        {value: 'today_active_tickets', viewValue: 'Today`s tickets'},
        {value: 'inactive_tickets', viewValue: 'Sold tickets history'},
      ];
    }

    this.getPurchasedTickets(this.selectedFilter);
  }

  getPurchasedTickets(filterValue: string) {
    this.myTickets = undefined;
    this.apiService.getPurchasedTickets({
      typeOfTickets: filterValue
    })
      .then((data: any) => {
        this.myTickets = data.tickets;
      }).catch((error) => {
      console.log(error);
    });
  }
}
