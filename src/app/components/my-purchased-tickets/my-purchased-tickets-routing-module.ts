import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {MyPurchasedTicketsComponent} from './my-purchased-tickets.component';

const routes: Routes = [
  {
    path: 'myPurchasedTickets', component: MyPurchasedTicketsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class MyPurchasedTicketsRoutingModule {
}
