import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPurchasedTicketsComponent } from './my-purchased-tickets.component';

describe('MyPurchasedTicketsComponent', () => {
  let component: MyPurchasedTicketsComponent;
  let fixture: ComponentFixture<MyPurchasedTicketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyPurchasedTicketsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPurchasedTicketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
