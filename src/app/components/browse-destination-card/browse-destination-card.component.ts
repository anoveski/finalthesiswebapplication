import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ApiService} from '../../services/api/api.service';
import {Router} from '@angular/router';
import {PassTicketDataService} from './pass-ticket-data/pass-ticket-data.service';

@Component({
  selector: 'app-browse-destination-card',
  templateUrl: './browse-destination-card.component.html',
  styleUrls: ['./browse-destination-card.component.css']
})
export class BrowseDestinationCardComponent implements OnInit {

  @Output() showTickets: EventEmitter<object> = new EventEmitter<object>();
  @Output() noAvailableTicketsForDestination: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() destinations: any;
  @Input() startLocation: any;
  @Input() destinationLocation: any;

  availableTickets: any[];

  constructor(private router: Router,
              private passTicketDataService: PassTicketDataService,
              private apiService: ApiService) {
  }

  ngOnInit() {
  }

  browseDestinations() {
    if (this.startLocation !== undefined && this.destinationLocation !== undefined) {

      this.showTickets.next(undefined);
      this.apiService.browseDestinations({
        startLocation: this.startLocation,
        destinationLocation: this.destinationLocation
      })
        .then((data: any[]) => {
          if (data.length === 0) {
            this.noAvailableTicketsForDestination.next(true);
          } else {
            this.availableTickets = data;
            this.noAvailableTicketsForDestination.next(false);
            if (this.router.url === '/home') {
              this.passTicketDataService.updateData({
                availableTickets: this.availableTickets,
                availableDestinations: this.destinations,
                noAvailableTickets: false,
                startLoc: this.startLocation,
                destinationLoc: this.destinationLocation
              });
              this.router.navigate(['home/browseTickets'], {});
            } else {
              this.showTickets.next(this.availableTickets);
            }
          }
        }).catch((e) => {
        console.log(e);
      });
    }
  }

  compareItems(i1, i2) {
    return i1 && i2 && i1._id === i2._id;
  }
}
