import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrowseDestinationCardComponent } from './browse-destination-card.component';

describe('BrowseDestinationCardComponent', () => {
  let component: BrowseDestinationCardComponent;
  let fixture: ComponentFixture<BrowseDestinationCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrowseDestinationCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrowseDestinationCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
