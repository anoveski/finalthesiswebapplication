import { TestBed } from '@angular/core/testing';

import { PassTicketDataService } from './pass-ticket-data.service';

describe('PassTicketDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PassTicketDataService = TestBed.get(PassTicketDataService);
    expect(service).toBeTruthy();
  });
});
