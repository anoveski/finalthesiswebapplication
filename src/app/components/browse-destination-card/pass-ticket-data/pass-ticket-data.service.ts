import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PassTicketDataService {

  private ticketsForSharing = new BehaviorSubject<any>('');
  tickets = this.ticketsForSharing.asObservable();

  constructor() { }

  updateData(tickets: any) {
    this.ticketsForSharing.next(tickets);
  }
}
