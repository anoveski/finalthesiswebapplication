import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit {

  @Input() ticket: any;

  pageId: string;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.pageId = this.route.snapshot.params.pageId;
    this.ticket = JSON.parse(this.route.snapshot.queryParams.ticketData);
  }
}
