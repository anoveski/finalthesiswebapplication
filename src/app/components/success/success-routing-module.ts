import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {SuccessComponent} from './success.component';

const routes: Routes = [
  {
    path: 'success/:pageId', component: SuccessComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class SuccessRoutingModule {
}
