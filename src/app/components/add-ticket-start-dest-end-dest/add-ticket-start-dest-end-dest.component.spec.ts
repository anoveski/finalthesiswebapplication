import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTicketStartDestEndDestComponent } from './add-ticket-start-dest-end-dest.component';

describe('AddTicketStartDestEndDestComponent', () => {
  let component: AddTicketStartDestEndDestComponent;
  let fixture: ComponentFixture<AddTicketStartDestEndDestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTicketStartDestEndDestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTicketStartDestEndDestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
