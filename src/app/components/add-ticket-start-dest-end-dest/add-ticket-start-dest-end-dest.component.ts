import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {countries} from '../../../additional-data/list-of-countries/countries';

@Component({
  selector: 'app-add-ticket-start-dest-end-dest',
  templateUrl: './add-ticket-start-dest-end-dest.component.html',
  styleUrls: ['./add-ticket-start-dest-end-dest.component.css']
})
export class AddTicketStartDestEndDestComponent implements OnInit {

  @Output() returnTicketStatus: EventEmitter<boolean> = new EventEmitter<boolean>();

  f: FormGroup;
  countriesList = countries;


  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.f = this.fb.group({
      startLocationCountryCode: new FormControl('', [
        Validators.required
      ]),
      endLocationCountryCode: new FormControl('', [
        Validators.required
      ]),
      startTownOrMunicipality: new FormControl('', [
        Validators.required
      ]),
      endTownOrMunicipality: new FormControl('', [
        Validators.required
      ]),
      oneWayTicket: new FormControl(false,
        Validators.required
      ),
      returnTicket: new FormControl(false,
        Validators.required
      ),
      oneWayPrice: new FormControl(null, [
        Validators.required
      ]),
      returnTicketPrice: new FormControl(null, [
        Validators.required
      ]),
    });
  }

  changeValue() {
    this.returnTicketStatus.next(this.f.get('returnTicket').value);
  }

  public getStartDestEndDestComponentData() {
    return {
      startLocation: {
        townOrMunicipality: this.f.controls.startTownOrMunicipality.value,
        country: this.countriesList.find(country => country.code === this.f.controls.startLocationCountryCode.value)
      },
      destinationLocation: {
        townOrMunicipality: this.f.controls.endTownOrMunicipality.value,
        country: this.countriesList.find(country => country.code === this.f.controls.endLocationCountryCode.value)
      },
      oneWayTicket: this.f.controls.oneWayTicket.value,
      returnTicket: this.f.controls.returnTicket.value,
      oneWayPrice: this.f.controls.oneWayPrice.value,
      returnTicketPrice: this.f.controls.returnTicketPrice.value,
    };
  }
}
