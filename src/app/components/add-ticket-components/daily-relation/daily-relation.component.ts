import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-daily-relation',
  templateUrl: './daily-relation.component.html',
  styleUrls: ['./daily-relation.component.css']
})
export class DailyRelationComponent implements OnInit {

  @Input() returnTicket: boolean;

  f: FormGroup;

  year = new Date().getFullYear();
  minDate = new Date(Date.now());
  maxDate = new Date(this.year + 4, 12, 31);

  constructor(private datePipe: DatePipe) {

    this.f = new FormGroup({
      selectDays: new FormArray([
        this.createDailyRelation(),
      ]),
    });
  }


  ngOnInit() {
  }

  createDailyRelation() {
    return new FormGroup({
      dayDate: new FormControl(''),
      departureHoursStart: new FormArray([
        this.createTimeItem()
      ]),
      departureHoursDestination:  new FormArray([
        this.createTimeItem()
      ])
    });
  }

  createTimeItem() {
    return new FormGroup({
      departureTime: new FormControl('')
    });
  }

  getSelectDays(form) {
    return form.controls.selectDays.controls;
  }

  getDepartureHours(form) {
    return form.controls.departureHoursStart.controls;
  }

  getDepartureDestinationHours(form) {
    return form.controls.departureHoursDestination.controls;
  }


  addDailyRelation(arrayName: string, i: number) {
    (this.f.controls[arrayName] as FormArray).push(this.createDailyRelation());

  }

  removeDailyRelation(arrayName: string, i: number) {
    (this.f.controls[arrayName] as FormArray).removeAt(i);
  }

  addTimeInput(form, arrayName: string, i: number) {
    (form.controls[arrayName] as FormArray).push(this.createTimeItem());
  }

  removeTimeInput(form, arrayName: string, i: number) {
    (form.controls[arrayName] as FormArray).removeAt(i);
  }

  takeDailyRelationResult() {
    return this.f.controls.selectDays.value;
  }
}
