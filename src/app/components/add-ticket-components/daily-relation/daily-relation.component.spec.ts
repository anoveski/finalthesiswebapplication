import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyRelationComponent } from './daily-relation.component';

describe('DailyRelationComponent', () => {
  let component: DailyRelationComponent;
  let fixture: ComponentFixture<DailyRelationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyRelationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyRelationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
