import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-every-day-relation',
  templateUrl: './every-day-relation.component.html',
  styleUrls: ['./every-day-relation.component.css']
})
export class EveryDayRelationComponent implements OnInit {

  f: FormGroup;
  @Input() returnTicket: boolean;

  constructor(private fb: FormBuilder) {
    console.log(this.returnTicket);
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.f = this.fb.group({
      departureHoursStart: this.fb.array([this.createTimeItem()]),
      departureHoursDestination: this.fb.array([this.createTimeItem()])
    });
  }

  addTimeInput(arrayName: string) {
    (this.f.controls[arrayName] as FormArray).push(this.createTimeItem());
  }

  removeTimeInput(arrayName: string, i: number) {
    (this.f.controls[arrayName] as FormArray).removeAt(i);
  }

  createTimeItem() {
    return this.fb.group({
      departureTime: ''
    });
  }

  takeEveryDayRelationResult() {
    return {
      departureHoursStart: this.f.controls.departureHoursStart.value,
      departureHoursDestination: this.f.controls.departureHoursDestination.value
    };
  }
}
