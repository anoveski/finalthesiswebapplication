import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EveryDayRelationComponent } from './every-day-relation.component';

describe('EveryDayRelationComponent', () => {
  let component: EveryDayRelationComponent;
  let fixture: ComponentFixture<EveryDayRelationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EveryDayRelationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EveryDayRelationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
