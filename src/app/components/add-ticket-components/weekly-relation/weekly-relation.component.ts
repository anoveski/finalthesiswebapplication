import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {GetWeekMondayPipe} from '../../../pipes/get-week-monday/get-week-monday.pipe';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-weekly-relation',
  templateUrl: './weekly-relation.component.html',
  styleUrls: ['./weekly-relation.component.css']
})
export class WeeklyRelationComponent implements OnInit {

  @Input() returnTicket: boolean;

  f: FormGroup;

  year = new Date().getFullYear();
  minDate = new Date(Date.now());
  maxDate = new Date(this.year + 4, 12, 31);

  constructor(private getWeekMonday: GetWeekMondayPipe,
              private datePipe: DatePipe) {

    this.f = new FormGroup({
      repeatEveryWeek: new FormControl(false, [
        Validators.required
      ]),
      selectedWeek: new FormArray([
        this.createDailyRelation(),
      ]),
    });
  }

  ngOnInit() {
  }

  addEvent(event) {
    if (event.value != null) {
      this.minDate = this.getWeekMonday.transform(event.value);
      this.maxDate = new Date(new Date(this.minDate).getTime() + (6 * 24 * 60 * 60 * 1000));
    } else {
      this.minDate = new Date(Date.now());
      this.maxDate = new Date(this.year + 4, 12, 31);
    }
  }

  createDailyRelation() {
    return new FormGroup({
      addEndDate: new FormControl(false,
        Validators.required
      ),
      startDate: new FormControl('', [
        Validators.required
      ]),
      endDate: new FormControl('', [
        Validators.required
      ]),
      departureHoursStart: new FormArray([
        this.createTimeItem()
      ]),
      departureHoursDestination: new FormArray([
        this.createTimeItem()
      ])
    });
  }

  createTimeItem() {
    return new FormGroup({
      departureTime: new FormControl('')
    });
  }

  getSelectedWeek(form) {
    return form.controls.selectedWeek.controls;
  }

  getDepartureHours(form) {
    return form.controls.departureHoursStart.controls;
  }

  getDepartureDestinationHours(form) {
    return form.controls.departureHoursDestination.controls;
  }

  addTimeInput(form, arrayName: string, i: number) {
    (form.controls[arrayName] as FormArray).push(this.createTimeItem());
  }

  addNewDailyRelation(form, arrayName: string, i: number) {
    (form.controls[arrayName] as FormArray).push(this.createDailyRelation());
  }

  removeDailyRelation(form, arrayName: string, i: number) {
    (form.controls[arrayName] as FormArray).removeAt(i);
  }

  removeTimeInput(form, arrayName: string, i: number) {
    (form.controls[arrayName] as FormArray).removeAt(i);
  }

  takeWeeklyRelationResult() {
    return {repeatEveryWeek: this.f.controls.repeatEveryWeek.value, selectedWeek: this.f.controls.selectedWeek.value};
  }
}
