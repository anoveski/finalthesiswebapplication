import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeeklyRelationComponent } from './weekly-relation.component';

describe('WeeklyRelationComponent', () => {
  let component: WeeklyRelationComponent;
  let fixture: ComponentFixture<WeeklyRelationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeeklyRelationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeeklyRelationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
