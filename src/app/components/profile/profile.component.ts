import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api/api.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profileData: any;
  typeOfUser: string;

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
    this.typeOfUser = localStorage.getItem('userRoles');

    this.apiService.getMyProfile()
      .then(data => {
        console.log(data);
        this.profileData = data;
      }).catch(e => {
      console.log(e);
    });
  }

}
