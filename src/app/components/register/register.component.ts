import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../../services/api/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  disableRegister = false;
  registerSuccess = false;
  clientRole: string;
  registerError = false;

  constructor(public route: ActivatedRoute,
              public router: Router,
              private apiService: ApiService) {
  }

  ngOnInit() {
    this.clientRole = this.route.snapshot.params.type;
  }

  signUp(credentials) {
    this.disableRegister = true;
    this.apiService.registerAccount(credentials)
      .then((data: any) => {
        if (data.success === true) {
          this.disableRegister = false;
          this.registerSuccess = true;
        } else {
          this.disableRegister = false;
          this.registerError = true;
        }
        console.log(data);
      })
      .catch((error) => {
        this.disableRegister = false;
        console.log(error);
      });
  }
}
