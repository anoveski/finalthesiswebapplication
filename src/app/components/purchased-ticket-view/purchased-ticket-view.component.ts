import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ApiService} from '../../services/api/api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-purchased-ticket-view',
  templateUrl: './purchased-ticket-view.component.html',
  styleUrls: ['./purchased-ticket-view.component.css']
})
export class PurchasedTicketViewComponent implements OnInit {

  @Input() ticketData: any;
  @Output() closeTicket: EventEmitter<object> = new EventEmitter<object>();

  constructor(private apiService: ApiService,
              public router: Router) {
  }

  ngOnInit() {
  }

  downloadTicket() {
    this.apiService.downloadPurchasedTicket({
      purchasedTicketId: this.ticketData._id
    }).then((data: any) => {
      const url = window.URL.createObjectURL(data.pdf);
      const anchor = document.createElement('a');
      anchor.download = data.fileName;
      anchor.href = url;
      anchor.click();
    }).catch((error) => {
      console.log(error);
    });
  }

  closePopover() {
    this.closeTicket.next();
  }

}
