import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasedTicketViewComponent } from './purchased-ticket-view.component';

describe('PurchasedTicketViewComponent', () => {
  let component: PurchasedTicketViewComponent;
  let fixture: ComponentFixture<PurchasedTicketViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasedTicketViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasedTicketViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
