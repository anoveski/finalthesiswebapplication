import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {BrowseTicketsComponent} from './browse-tickets.component';

const routes: Routes = [
  {
    path: 'home/browseTickets', component: BrowseTicketsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class BrowseTicketsRoutingModule {
}
