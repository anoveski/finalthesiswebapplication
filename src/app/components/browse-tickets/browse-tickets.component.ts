import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PassTicketDataService} from '../browse-destination-card/pass-ticket-data/pass-ticket-data.service';

@Component({
  selector: 'app-browse-tickets',
  templateUrl: './browse-tickets.component.html',
  styleUrls: ['./browse-tickets.component.css']
})
export class BrowseTicketsComponent implements OnInit {

  availableTickets: any[];
  availableDestinations: any[];
  noAvailableTickets: boolean;
  startLocSelected: any;
  destinationLocSelected: any;

  constructor(private route: ActivatedRoute,
              private passTicketDataService: PassTicketDataService) {

    const subscription = this.passTicketDataService.tickets
      .subscribe((data: any) => {
        this.availableTickets = data.availableTickets;
        this.availableDestinations = data.availableDestinations;
        this.noAvailableTickets = data.noAvailableTickets;
        this.startLocSelected = data.startLoc;
        this.destinationLocSelected = data.destinationLoc;
      });

    setTimeout(() => subscription.unsubscribe(), 1000);
  }

  ngOnInit() {
  }

  geAvailableTicketsValue(event) {
    this.availableTickets = [];
    this.noAvailableTickets = event;
  }

  listTickets(event) {
    this.availableTickets = event;
  }
}
