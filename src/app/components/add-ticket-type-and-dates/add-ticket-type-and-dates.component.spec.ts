import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTicketTypeAndDatesComponent } from './add-ticket-type-and-dates.component';

describe('AddTicketTypeAndDatesComponent', () => {
  let component: AddTicketTypeAndDatesComponent;
  let fixture: ComponentFixture<AddTicketTypeAndDatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTicketTypeAndDatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTicketTypeAndDatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
