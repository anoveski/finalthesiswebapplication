import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {WeeklyRelationComponent} from '../add-ticket-components/weekly-relation/weekly-relation.component';
import {DailyRelationComponent} from '../add-ticket-components/daily-relation/daily-relation.component';
import {EveryDayRelationComponent} from '../add-ticket-components/every-day-relation/every-day-relation.component';

@Component({
  selector: 'app-add-ticket-type-and-dates',
  templateUrl: './add-ticket-type-and-dates.component.html',
  styleUrls: ['./add-ticket-type-and-dates.component.css']
})
export class AddTicketTypeAndDatesComponent implements OnInit {

  @Input() returnTicket: boolean;

  @ViewChild(WeeklyRelationComponent) weeklyRelation;
  @ViewChild(DailyRelationComponent) everyDayRelation;
  @ViewChild(EveryDayRelationComponent) dailyRelation;

  f: FormGroup;
  typeOfTransportVehicles: any;
  typesOfRelation: any;

  constructor(private fb: FormBuilder) {
    this.typeOfTransportVehicles = [
      {
        value: 'Bus',
        label: 'Bus'
      }, {
        value: 'Minibus',
        label: 'Minibus'
      }, {
        value: 'Train',
        label: 'Train'
      }, {
        value: 'Car',
        label: 'Car'
      }, {
        value: 'Other',
        label: 'Other'
      }];

    this.typesOfRelation = [
      {
        value: 'weekly',
        label: 'Weekly'
      },
      {
        value: 'select_days',
        label: 'Select days'
      },
      {
        value: 'every_day',
        label: 'Every day'
      }
    ];
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.f = this.fb.group({
      availableSeats: new FormControl(null, [
        Validators.required
      ]),
      typeOfTransportationVehicle: new FormControl('', [
        Validators.required
      ]),
      typeOfRelation: new FormControl('', [
        Validators.required
      ]),
    });
  }

  public getTypeAndDatesComponentData() {
    return {
      availableSeats: this.f.controls.availableSeats.value,
      typeOfTransportationVehicle: this.f.controls.typeOfTransportationVehicle.value,
      typeOfRelation: this.f.controls.typeOfRelation.value,
      relationData: {
        weeklyRelation: (this.f.controls.typeOfRelation.value === 'weekly' ? this.weeklyRelation.takeWeeklyRelationResult() : null),
        dailyRelation: (this.f.controls.typeOfRelation.value === 'select_days' ? this.everyDayRelation.takeDailyRelationResult() : null),
        everyDayRelation: (this.f.controls.typeOfRelation.value === 'every_day' ? this.dailyRelation.takeEveryDayRelationResult() : null)
      }
    };
  }
}
