import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css'],
})

export class SliderComponent implements OnInit {

  public sliderImages: string[] = [
    '../../../assets/home-slider-imgs/slider-photo-1.jpg',
    '../../../assets/home-slider-imgs/slider-photo-2.jpg',
    '../../../assets/home-slider-imgs/slider-photo-3.jpg',
    '../../../assets/home-slider-imgs/slider-photo-4.jpg'
  ];

  constructor() {
  }

  ngOnInit() {
  }
}
