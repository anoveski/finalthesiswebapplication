import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackPersonalVehicleComponent } from './track-personal-vehicle.component';

describe('TrackPersonalVehicleComponent', () => {
  let component: TrackPersonalVehicleComponent;
  let fixture: ComponentFixture<TrackPersonalVehicleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackPersonalVehicleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackPersonalVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
