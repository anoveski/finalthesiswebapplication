import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-track-personal-vehicle',
  templateUrl: './track-personal-vehicle.component.html',
  styleUrls: ['./track-personal-vehicle.component.css']
})
export class TrackPersonalVehicleComponent implements OnInit {

  @Output() browsePersonalVehicles: EventEmitter<object> = new EventEmitter<object>();

  f: FormGroup;
  myVehicles: any = [
    {
      name: 'Vehicle 1'
    },
    {
      name: 'Vehicle 2'
    },
    {
      name: 'Vehicle 3'
    },
    {
      name: 'Vehicle 4'
    }
  ];

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.f = this.fb.group({
      selectedVehicle: new FormControl('', [
        Validators.required
      ]),
    });
  }

  searchPersonalVehicle() {
    this.browsePersonalVehicles.next({
      selectedVehicle: this.f.controls.selectedVehicle.value
    });
  }
}
