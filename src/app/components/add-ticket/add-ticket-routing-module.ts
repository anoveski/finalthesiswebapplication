import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AddTicketComponent} from './add-ticket.component';

const routes: Routes = [
  {
    path: 'addTicket', component: AddTicketComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AddTicketRoutingModule {
}
