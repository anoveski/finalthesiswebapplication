import {Component, OnInit, ViewChild} from '@angular/core';
import {ApiService} from '../../services/api/api.service';
import {Router} from '@angular/router';
import {AddTicketTypeAndDatesComponent} from '../add-ticket-type-and-dates/add-ticket-type-and-dates.component';
import {AddTicketStartDestEndDestComponent} from '../add-ticket-start-dest-end-dest/add-ticket-start-dest-end-dest.component';

@Component({
  selector: 'app-add-ticket',
  templateUrl: './add-ticket.component.html',
  styleUrls: ['./add-ticket.component.css']
})
export class AddTicketComponent implements OnInit {

  @ViewChild(AddTicketTypeAndDatesComponent) typeAndDates;
  @ViewChild(AddTicketStartDestEndDestComponent) startDestEndDest;

  disableSubmit = false;
  returnTicketValue = false;

  constructor(private apiService: ApiService,
              private router: Router) {
  }

  ngOnInit() {
  }

  submit() {
    this.addTicket(Object.assign({},
      this.typeAndDates.getTypeAndDatesComponentData(), this.startDestEndDest.getStartDestEndDestComponentData()));
  }

  addTicket(ticketData) {
    this.disableSubmit = true;
    this.apiService.addTicket(ticketData)
      .then((data: any) => {
        this.disableSubmit = false;
        console.log(data);
        if (data.success === true) {
          this.router.navigate(['success/addTicket'], {
            queryParams: {
              ticketData: true
            }
          });
        }
      }).catch(error => {
      this.disableSubmit = false;
      console.log(error);
    });
  }

  ticketStatus(event) {
    this.returnTicketValue = event;
  }
}
