import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogPurchaseTicketComponent } from './dialog-purchase-ticket.component';

describe('DialogPurchaseTicketComponent', () => {
  let component: DialogPurchaseTicketComponent;
  let fixture: ComponentFixture<DialogPurchaseTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogPurchaseTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogPurchaseTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
