import {Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormGroup} from '@angular/forms';
import {StepSelectTicketComponent} from './steps/step-select-ticket/step-select-ticket.component';

@Component({
  selector: 'app-dialog-purchase-ticket',
  templateUrl: './dialog-purchase-ticket.component.html',
  styleUrls: ['./dialog-purchase-ticket.component.css']
})
export class DialogPurchaseTicketComponent implements OnInit, OnDestroy {

  @ViewChild(StepSelectTicketComponent) stepSelectTicketComponent;

  isLinear = true;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  selectTicketData: any;
  enterPaymentData: any;
  ticketData: any;
  typeOfTicket: string;

  constructor(public dialogRef: MatDialogRef<DialogPurchaseTicketComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.ticketData = this.data.ticketData;
    this.typeOfTicket = this.data.typeOfTicket;
  }

  ngOnDestroy() {
  }

  getSelectTicketData(event) {
    this.selectTicketData = event;
  }

  getEnterPaymentData(event) {
    this.enterPaymentData = event;
  }
}
