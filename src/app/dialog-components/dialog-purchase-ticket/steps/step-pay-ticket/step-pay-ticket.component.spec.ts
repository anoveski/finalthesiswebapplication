import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepPayTicketComponent } from './step-pay-ticket.component';

describe('StepPayTicketComponent', () => {
  let component: StepPayTicketComponent;
  let fixture: ComponentFixture<StepPayTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepPayTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepPayTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
