import {Component, Input, OnChanges, SimpleChanges, ViewChild} from '@angular/core';
import {ApiService} from '../../../../services/api/api.service';
import {MatDialog} from '@angular/material';
import {DialogConfirmationComponent} from '../../../dialog-confirmation/dialog-confirmation.component';
import {SpinnerComponent} from '../../../../components/spinner/spinner.component';
import {Router} from '@angular/router';
import {BILLING_CURRENCY} from '../../../../../environments/environment';

@Component({
  selector: 'app-step-pay-ticket',
  templateUrl: './step-pay-ticket.component.html',
  styleUrls: ['./step-pay-ticket.component.css']
})
export class StepPayTicketComponent implements OnChanges {

  @ViewChild(SpinnerComponent) spinnerComponent;

  @Input('enterPaymentData') enterPaymentData: any;
  @Input('selectTicketData') selectTicketData: any;

  currency: string = BILLING_CURRENCY.currency;

  passengerData: any;
  ticketDetails: any;
  ticketType: string;
  selectedDeparture: any;
  totalPrice: number;
  confirmTicket = false;

  constructor(private apiService: ApiService,
              public router: Router,
              public dialog: MatDialog) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.selectTicketData !== undefined) {
      this.selectedDeparture = this.selectTicketData.selectedDeparture;
      this.passengerData = this.selectTicketData.userData;
      this.ticketDetails = this.selectTicketData.ticketDetails;
      this.ticketType = this.selectTicketData.ticketType;
    }

    if (this.ticketType === 'oneWayTicket') {
      this.totalPrice = this.ticketDetails.oneWayPrice;
    } else if (this.ticketType === 'returnTicket') {
      this.totalPrice = this.ticketDetails.returnTicketPrice;
    }
  }


  confirmAndPayTicket() {
    const dialogRef = this.dialog.open(DialogConfirmationComponent, {
      width: '250px',
      data: {
        title: 'Buy ticket', comment: 'Are you sure you want to purchase this ticket?',
        confirmButton: 'Yes', denyButton: 'No', confirmStatus: this.confirmTicket
      }
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === true) {
        this.spinnerComponent.showSpinner();
        this.apiService.purchaseTicket({
          stripeUserPurchaseTicket: this.enterPaymentData,
          ticketId: this.ticketDetails._id,
          ticketType: this.ticketType,
          passenger: this.passengerData,
          selectedDeparture: this.selectedDeparture
        })
          .then((data: any) => {
            this.spinnerComponent.closeSpinner();
            this.dialog.closeAll();
            this.router.navigate(['success/purchaseTicket'], {
              queryParams: {
                ticketData: JSON.stringify(data.purchasedTicketData)
              }
            });
          }).catch((error) => {
          console.log(error);
          this.spinnerComponent.closeSpinner();
        });
      }
    });
  }
}
