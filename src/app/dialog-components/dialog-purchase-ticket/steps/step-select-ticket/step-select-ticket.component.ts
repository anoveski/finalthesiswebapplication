import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {getDates} from '../../../../helper-methods/get-dates-between/get-dates-between';
import {DatePipe} from '@angular/common';
import {BILLING_CURRENCY} from '../../../../../environments/environment';

@Component({
  selector: 'app-step-select-ticket',
  templateUrl: './step-select-ticket.component.html',
  styleUrls: ['./step-select-ticket.component.css']
})
export class StepSelectTicketComponent implements OnInit {

  @Input() ticketDetails: any;
  @Input() ticketType: any;

  @Output() passSelectTicketData: EventEmitter<object> = new EventEmitter<object>();

  f: FormGroup;

  currency: string = BILLING_CURRENCY.currency;

  weeklyRelation: any[];
  dailyRelation: any[];
  everyDayRelation: any;

  weeklyRelationDatesAndHours: any[];
  weeklyRelationAvailableDays: any[] = [];

  availableDepartureDates: any;

  availableStartDepartureHours: any;
  availableReturnDepartureHours: any;

  // ToDo: Select ticket
  departureFilter = (d: Date): boolean => {
    const now = new Date();

    if (this.ticketDetails.typeOfRelation === 'weekly') {

      if (this.ticketDetails.relationData.weeklyRelation.repeatEveryWeek === true) {
        if (this.weeklyRelation[0].startDate > now.toISOString()) {
          return (d >= new Date(this.weeklyRelation[0].startDate))
            && this.weeklyRelationAvailableDays.some(day => day === this.datePipe.transform(d, 'EEE'));
        } else {
          return (d >= now) && this.weeklyRelationAvailableDays.some(day => day === this.datePipe.transform(d, 'EEE'));
        }
      } else {
        return this.availableDepartureDates[d.toISOString()];
      }

    } else if (this.ticketDetails.typeOfRelation === 'select_days') {
      return this.availableDepartureDates[d.toISOString()];
    } else if (this.ticketDetails.typeOfRelation === 'every_day') {
      return (now <= d);
    }
  };

  constructor(private fb: FormBuilder,
              private datePipe: DatePipe) {
    this.f = this.fb.group({
      startLocationDate: new FormControl('', [
        Validators.required
      ]),
      startLocationHour: new FormControl('', [
        Validators.required
      ]),
      returnLocationDate: new FormControl('', [
        Validators.required
      ]),
      returnLocationHour: new FormControl('', [
        Validators.required
      ]),
    });
  }

  ngOnInit() {
    this.organizeTicketData();
  }

  organizeTicketData() {
    if (this.ticketDetails.typeOfRelation === 'weekly') {

      this.weeklyRelation = this.ticketDetails.relationData.weeklyRelation.selectedWeek;

      this.weeklyRelationDatesAndHours = this.weeklyRelation.map((data) => {
        let weeklyRelationDates;
        if (data.endDate === '') {
          weeklyRelationDates = [new Date(data.startDate)];
        } else {
          weeklyRelationDates = getDates(new Date(data.startDate), new Date(data.endDate));
        }


        return {
          availableDates: weeklyRelationDates,
          availableHours: {
            departureHoursStart: data.departureHoursStart,
            departureHoursDestination: data.departureHoursDestination
          }
        };
      });

      const dates = this.weeklyRelationExtractDates(this.weeklyRelationDatesAndHours);

      this.availableDepartureDates = this.decodeMappedDates(dates.departureDate);

      if (this.ticketDetails.relationData.weeklyRelation.repeatEveryWeek === true) {
        Object.keys(this.availableDepartureDates).forEach((key) => {
          if (this.availableDepartureDates[key] === true) {
            this.weeklyRelationAvailableDays.push(this.datePipe.transform(key, 'EEE'));
          }
        });
      }

    } else if (this.ticketDetails.typeOfRelation === 'select_days') {

      this.dailyRelation = this.ticketDetails.relationData.dailyRelation;

      const filteredDailyRelation = this.dailyRelation.filter(relation => {
        if (relation.dayDate > (this.datePipe.transform(new Date(), 'yyyy-MM-dd') + 'T22:00:00.000Z')) {
          return relation;
        }
      });

      const dates = this.selectDaysExtractDates(filteredDailyRelation);
      this.availableDepartureDates = this.decodeMappedDates(dates.departureDate);

    } else if (this.ticketDetails.typeOfRelation === 'every_day') {
      this.everyDayRelation = this.ticketDetails.relationData.everyDayRelation;

    }
  }

  weeklyRelationExtractDates(weeklyDaysArray) {
    const departureDateObj = new Map();

    weeklyDaysArray.forEach(day => {
      day.availableDates.forEach(d => {
        departureDateObj.set(d.toISOString(), true);
      });
    });

    return {departureDate: departureDateObj};
  }

  selectDaysExtractDates(selectDaysArray) {
    const departureDateObj = new Map();

    selectDaysArray.map((data) => {
      departureDateObj.set(data.dayDate, true);
    });

    return {departureDate: departureDateObj};
  }

  decodeMappedDates(departureDateObj) {
    return Array.from(departureDateObj).reduce((obj, [key, value]) => (
      Object.assign(obj, {[key]: value})
    ), {});
  }

  selectedDate(ev) {
    const selectedDate: Date = ev.value.toISOString();

    if (this.ticketDetails.typeOfRelation === 'weekly') {
      this.weeklyRelationDatesAndHours.forEach(data => {

        data.availableDates.forEach(date => {
          if (this.datePipe.transform(date, 'EEE') === this.datePipe.transform(selectedDate, 'EEE')) {
            if (ev.targetElement.name === 'startLocationDate') {
              this.availableStartDepartureHours = data.availableHours.departureHoursStart;
            } else if (ev.targetElement.name === 'returnLocationDate') {
              this.availableReturnDepartureHours = data.availableHours.departureHoursDestination;
            }
          }
        });

      });

    } else if (this.ticketDetails.typeOfRelation === 'select_days') {
      this.dailyRelation.forEach(data => {
        if (selectedDate === data.dayDate) {
          if (ev.targetElement.name === 'startLocationDate') {
            this.availableStartDepartureHours = data.departureHoursStart;
          } else if (ev.targetElement.name === 'returnLocationDate') {
            this.availableReturnDepartureHours = data.departureHoursDestination;
          }
        }
      });
    } else if (this.ticketDetails.typeOfRelation === 'every_day') {
      if (ev.targetElement.name === 'startLocationDate') {
        this.availableStartDepartureHours = this.everyDayRelation.departureHoursStart;
      } else if (ev.targetElement.name === 'returnLocationDate') {
        this.availableReturnDepartureHours = this.everyDayRelation.departureHoursDestination;
      }
    }
  }

  goToCheckout() {
    this.passSelectTicketData.next({
      userData: JSON.parse(localStorage.getItem('userData')),
      ticketDetails: this.ticketDetails,
      ticketType: this.ticketType,
      selectedDeparture: {
        startLocationDate: this.f.controls.startLocationDate.value,
        startLocationHour: this.f.controls.startLocationHour.value,
        returnLocationDate: this.f.controls.returnLocationDate.value,
        returnLocationHour: this.f.controls.returnLocationHour.value
      }
    });
  }
}
