import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepSelectTicketComponent } from './step-select-ticket.component';

describe('StepSelectTicketComponent', () => {
  let component: StepSelectTicketComponent;
  let fixture: ComponentFixture<StepSelectTicketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepSelectTicketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepSelectTicketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
