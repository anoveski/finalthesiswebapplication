import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepEnterPaymentComponent } from './step-enter-payment.component';

describe('StepEnterPaymentComponent', () => {
  let component: StepEnterPaymentComponent;
  let fixture: ComponentFixture<StepEnterPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepEnterPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepEnterPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
