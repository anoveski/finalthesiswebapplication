import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {STRIPE_PUBLIC_KEY} from '../../../../../environments/environment';
import {SpinnerComponent} from '../../../../components/spinner/spinner.component';

declare var Stripe: any;

@Component({
  selector: 'app-step-enter-payment',
  templateUrl: './step-enter-payment.component.html',
  styleUrls: ['./step-enter-payment.component.css']
})
export class StepEnterPaymentComponent implements OnInit {

  @ViewChild(SpinnerComponent) spinnerComponent;

  @Input() stepper: any;
  @Input() selectTicketData: any;

  @Output() enterPaymentData: EventEmitter<object> = new EventEmitter<object>();

  @ViewChild('cardElement') cardElement: ElementRef;

  stripe;
  card;
  cardErrors;

  constructor() {
  }

  ngOnInit() {
    this.stripe = Stripe(STRIPE_PUBLIC_KEY.stripePublicKey);
    const elements = this.stripe.elements();

    this.card = elements.create('card');
    this.card.mount(this.cardElement.nativeElement);

    this.card.addEventListener('change', ({error}) => {
      this.cardErrors = error && error.message;
    });
  }

  async handleForm(event) {
    this.spinnerComponent.showSpinner();
    event.preventDefault();

    const {source, error} = await this.stripe.createSource(this.card);

    if (error) {
      // Inform the customer that there was an error.
      const cardErrors = error.message;
      this.spinnerComponent.closeSpinner();
    } else {
      this.spinnerComponent.closeSpinner();
      this.stepper.next();
      this.enterPaymentData.next(source.id);
    }
  }
}
