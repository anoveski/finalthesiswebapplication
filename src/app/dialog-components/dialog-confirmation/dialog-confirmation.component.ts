import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-dialog-confirmation',
  templateUrl: './dialog-confirmation.component.html',
  styleUrls: ['./dialog-confirmation.component.css']
})
export class DialogConfirmationComponent implements OnInit {

  title: string;
  comment: string;
  confirmButton: string;
  denyButton: string;

  constructor(public dialogRef: MatDialogRef<DialogConfirmationComponent>,
              public dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: any) {

    this.title = data.title;
    this.comment = data.comment;
    this.confirmButton = data.confirmButton;
    this.denyButton = data.denyButton;
  }

  ngOnInit() {
  }

  confirm() {
    return this.data.confirmStatus = true;
  }

  deny(): void {
    this.dialogRef.close();
  }
}
