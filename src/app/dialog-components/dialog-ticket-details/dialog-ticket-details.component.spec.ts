import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogTicketDetailsComponent } from './dialog-ticket-details.component';

describe('DialogTicketDetailsComponent', () => {
  let component: DialogTicketDetailsComponent;
  let fixture: ComponentFixture<DialogTicketDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogTicketDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogTicketDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
