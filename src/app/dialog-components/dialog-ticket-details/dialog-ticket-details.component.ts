import {Component, Inject, OnInit} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {DialogPurchaseTicketComponent} from '../dialog-purchase-ticket/dialog-purchase-ticket.component';
import {BILLING_CURRENCY} from '../../../environments/environment';

@Component({
  selector: 'app-dialog-ticket-details',
  templateUrl: './dialog-ticket-details.component.html',
  styleUrls: ['./dialog-ticket-details.component.css']
})
export class DialogTicketDetailsComponent implements OnInit {

  chosenTicket = 'oneWayTicket';
  typeOfUser: string;
  currency: string = BILLING_CURRENCY.currency;

  constructor(public dialogRef: MatDialogRef<DialogTicketDetailsComponent>,
              public dialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.typeOfUser = localStorage.getItem('userRoles');
  }

  purchaseTicket() {

    const dialogRef = this.dialog.open(DialogPurchaseTicketComponent, {
      height: '70%',
      width: '70%',
      data: {typeOfTicket: this.chosenTicket, ticketData: this.data}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

