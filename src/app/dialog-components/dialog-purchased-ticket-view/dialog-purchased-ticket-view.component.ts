import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-dialog-purchased-ticket-view',
  templateUrl: './dialog-purchased-ticket-view.component.html',
  styleUrls: ['./dialog-purchased-ticket-view.component.css']
})
export class DialogPurchasedTicketViewComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DialogPurchasedTicketViewComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
  }

  closeDialog(event) {
    this.dialogRef.close();
  }
}
