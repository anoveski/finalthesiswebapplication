import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogPurchasedTicketViewComponent } from './dialog-purchased-ticket-view.component';

describe('DialogPurchasedTicketViewComponent', () => {
  let component: DialogPurchasedTicketViewComponent;
  let fixture: ComponentFixture<DialogPurchasedTicketViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogPurchasedTicketViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogPurchasedTicketViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
